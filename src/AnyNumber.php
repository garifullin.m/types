<?php

declare(strict_types=1);

namespace MAGarif\Types;

use MAGarif\Types\Contracts\Numeric;

final class AnyNumber implements Numeric
{
    public function __construct(private int|float $value) {}

    public function value(): int|float
    {
        return $this->value;
    }
}
