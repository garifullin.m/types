<?php

declare(strict_types=1);

namespace MAGarif\Types;

class Money
{
    public function __construct(protected float $value, protected string $currency)
    {

    }
}
