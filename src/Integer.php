<?php

declare(strict_types=1);

namespace MAGarif\Types;

use InvalidArgumentException;
use MAGarif\Types\Contracts\Numeric;

class Integer implements Numeric
{
    public function __construct(private Numeric $number) {}

    public function value(): int
    {
        if (!is_int($this->number->value())) {
            throw new InvalidArgumentException('');
        }

        return $this->number->value();
    }
}
