<?php

declare(strict_types=1);

namespace MAGarif\Types;

use InvalidArgumentException;
use MAGarif\Types\Contracts\Numeric;

final class UnsignedNumber implements Numeric
{
    public function __construct(private Numeric $number) {}

    public function value(): int|float
    {
        if ($this->number->value() < 0) {
            throw new InvalidArgumentException('');
        }

        return $this->number->value();
    }
}
