<?php

declare(strict_types=1);

namespace MAGarif\Types\Contracts;

interface Scalar
{
    public function value(): int|float|string|bool;
}
