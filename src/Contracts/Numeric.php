<?php

declare(strict_types=1);

namespace MAGarif\Types\Contracts;

interface Numeric extends Scalar
{
    public function value(): int|float;
}
