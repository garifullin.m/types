<?php

declare(strict_types=1);

namespace MAGarif\Types;

use MAGarif\Types\Contracts\Scalar;

final class Text implements Scalar
{
    public function __construct(protected string $value) {}

    public function value(): string
    {
        return $this->value;
    }
}
